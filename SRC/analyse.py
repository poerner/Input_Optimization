import sys
import os
import numpy as np
import matplotlib.pyplot as plt

import scipy.stats

def get_scores(f):
    scores = {}
    with open(f) as handle:
        for line in handle:
            if line.startswith("OPT"):
                splitline = line.strip().split("\t")
                scores[int(splitline[1])] = float(splitline[4])
    return scores


def analyse(files, exclude_deficient = True):
    scores = {}
    for f in files:
        scores[f] = get_scores(f)
        if os.path.exists(f + "_rev"):
            scores[f].update(get_scores(f + "_rev"))
        for i in range(80):
            if os.path.exists(f + "_" + str(i)):
                scores[f].update(get_scores(f + "_" + str(i)))

    if exclude_deficient:
        indices = [set(scores[s].keys()) for s in scores]
        no_exclude = set.intersection(*indices)
        scores = {f: {idx: scores[f][idx] for idx in scores[f] if idx in no_exclude} for f in scores}

    return scores

if __name__ == "__main__":
    NAMES = ["full", "embfree", "logits", "softmax", "gumbel"]
    NAMESMAP = {"full": "crp", "embfree": "emb", "softmax": "smx", "gumbel": "gbl", "logits": "logit"}
   
    plt.figure(figsize = (3,2))
    plt.rc('font', size=7, family = "serif")
    for n, which in zip((211, 212), ("I", "TL")):
        files = [os.path.join("../Logs", name + "_" + which + "_P") for name in NAMES]
        scores = analyse(files)

        scores = {name: [scores[f][idx] for idx in sorted(list(scores[f].keys()))] for f, name in zip(files, NAMES)}

        plt.subplot(n)
        labels = [NAMESMAP.get(name, name) for name in NAMES]
        plt.boxplot(list(reversed([scores[name] for name in NAMES])), sym = '', vert = False, labels = list(reversed(labels)))

        print(which, len(scores[NAMES[0]]))
        if which == "I":
            plt.title("visual model (n=" + str(len(scores[NAMES[0]])) + ")")
        else:
            plt.title("language model (n=" + str(len(scores[NAMES[0]])) + ")")

        #print(scipy.stats.ttest_rel(scores["grid"], scores["gumbel"]))
    
    plt.tight_layout()
    plt.savefig("../figure_test.png", dpi = 300)
    #plt.show()

    
