import numpy as np
np.random.seed(123)

import os
import argparse
import _pickle

from keras.layers import Layer, Embedding, Dense, Lambda, Input, TimeDistributed, Activation
from keras.models import Sequential, Model, load_model
from keras.callbacks import Callback, CSVLogger, EarlyStopping, ProgbarLogger
import keras.backend as K
import keras.losses

from progressbar import ProgressBar
from sklearn.metrics.pairwise import cosine_similarity

MODELPATH = "../models/I_T_train.hdf5"
DICTIONARYPATH = "../dictionary.pkl"
DATAPATH = "../data/coco/vendrov/data/coco/"
CUSTOM_OBJECTS = {"<lambda>": lambda y_true, y_pred: 1 + keras.losses.cosine_proximity(y_true, y_pred)}
CSVDIR = "../CSV"
WEIGHTDIR = "../weights"

def maximizer_loss(y_true, y_pred):
	return -K.mean(y_pred)

def report_loss(y_true, y_pred):
	return K.mean(y_pred)


class KeepBestModelCallback(Callback):
	def __init__(self, monitor, mode = "auto", verbose = 1):
		self.monitor = monitor
		self.verbose = verbose

		if mode not in ["auto", "min", "max"]:
			raise Exception("Mode must be in 'auto', 'min', 'max'")

		if mode == "auto":	
			if "acc" in self.monitor or "fmeasure" in self.monitor:
                		mode = "max"
			elif "loss" in self.monitor:
				mode = "min"
			else:
				raise Exception("Cannot infer mode from", self.monitor)
	
		if mode == "min":
			self.monitor_op = np.less
			self.best = np.Inf
		elif mode == "max":
			self.monitor_op = np.greater
			self.best = -np.Inf

	def on_train_begin(self, logs = None):
		self.weights = self.model.get_weights()
		
	def on_epoch_end(self, epoch, logs = None):
		current = logs.get(self.monitor)
		if current is None:
			raise Exception("Cannot monitor", self.monitor, "as it is not present in the model logs")
			
		if self.monitor_op(current, self.best):
			if self.verbose > 0:
				print('\nEpoch %05d: %s improved from %0.5f to %0.5f\n' % (epoch+1, self.monitor, self.best, current))
			self.weights = self.model.get_weights()
			self.best = current
		
	def on_train_end(self, logs = None):
		self.model.set_weights(self.weights)


		
class BestStringCallback(Callback):
	def __init__(self, optimizer, generator):
		self.optimizer = optimizer
		self.rev_dictionary = optimizer.rev_dictionary
		self.model = optimizer.model
		self.generator = generator
		self.name_to_index = {name: i for i, name in enumerate(self.model.output_names)}
	
	def on_epoch_end(self, epoch, logs):
		tmp = self.model.predict_on_batch(next(self.generator)[0])
		if "t" in self.name_to_index:
			print("Temperature:", tmp[self.name_to_index["t"]][0].squeeze())
		
		statistics = []
		for name in sorted(list(self.name_to_index.keys())):
			if name in ("o_softmax", "o_logits", "o_argmax", "o_gumbel"):
				outputs = tmp[self.name_to_index[name]]
				if name == "o_gumbel":
					outputs = outputs[:5]
				else:
					outputs = outputs[:1]

				for output in outputs:
					statistics.append((name, output.argmax(axis = -1), output.max(axis = -1)))

			elif name == "o_emb":
				output = tmp[self.name_to_index[name]][0]
				similarities = cosine_similarity(output, self.optimizer.weights["emb"][0])
				statistics.append((name, similarities.argmax(axis = -1), similarities.max(axis = -1)))
		
		for name, words, stats in statistics:
			print(name, end = ": ")
			print(" ".join(self.rev_dictionary[w] + " (" + str(round(s, 4)) + ")" for w, s in zip(words, stats)))
	
	
class SnapToClosest(Layer):
	def __init__(self, reference, mode, **kwargs):
		super(SnapToClosest, self).__init__(**kwargs)
		self.reference = reference
		if mode == "min":
			self.op = K.argmin
		elif mode == "max":
			self.op = K.argmax
		else:
			raise Exception()

	def call(self, inputs):
		positions = self.op(inputs, axis = -1)
		return K.gather(self.reference, positions)
	
class PairwiseCosines(Layer):
	def __init__(self, reference, **kwargs):
		super(PairwiseCosines, self).__init__(**kwargs)
		self.reference_norm = K.expand_dims(K.l2_normalize(reference, axis = -1), -3) # (..., 1, vocab, emb)
	
	def call(self, inputs):
		inputs_norm = K.expand_dims(K.l2_normalize(inputs, axis = -1), -2) # (..., length, 1, emb)
		return K.sum(inputs_norm * self.reference_norm, axis = -1) # (..., length, vocab)

class Name(Layer):
	def __init__(self, name, **kwargs):
		super(Name, self).__init__(name = name, **kwargs)


class TemperatureLayer(Layer):
	def __init__(self, temperature = None, **kwargs):
		super(TemperatureLayer, self).__init__(**kwargs)
		self.temperature = temperature
	
	def compute_output_shape(self, input_shape):
		if self.temperature is None:
			if not (isinstance(input_shape, list) and len(input_shape) >= 2):
				raise Exception("If you do not specify a temperature parameter, it is expected as last input")
			input_shape = input_shape[:-1]
		
		output_shape = self._compute_output_shape(input_shape)
		if len(output_shape) == 1:
			return output_shape[0]
		return output_shape
	
	def call(self, inputs):
		if self.temperature is None:
			if not (isinstance(inputs, list) and len(inputs) >= 2):
				raise Exception("If you do not specify a temperature parameter, it is expected as last input")
			
			self._temperature = inputs[-1]
			inputs = inputs[:-1]
		else:
			self._temperature = self.temperature

		if not hasattr(inputs, "__len__"):
			inputs = [inputs]

		outputs = self._call(inputs)
		if len(outputs) == 1:
			return outputs[0]
		return outputs


class Gumbel(TemperatureLayer):
	def _call(self, inputs):
		outputs = []

		for inp in inputs:
			gumbel_noise = -K.log(-K.log(K.random_uniform(shape = K.shape(inp), minval = 0.0, maxval = 1.0)))
			
			temperature = self._temperature
			for _ in range(K.ndim(inp) - K.ndim(self._temperature)):
				temperature = K.expand_dims(temperature, -1)

			outputs.append(K.softmax((K.log(inp + K.epsilon()) + gumbel_noise) / temperature, -1))
		
		return outputs
	
	def _compute_output_shape(self, input_shape):
		return input_shape		

class ProximityRegularizationLayer(TemperatureLayer):
	def _call(self, inputs):
		if len(inputs) != 2:
			raise Exception("Proximity Regularization Layer expects two inputs (plus optional temperature parameter)")
	
		temperature = self._temperature 
		
		for _ in range(K.ndim(inputs[0]) - K.ndim(self._temperature)):
			temperature = K.expand_dims(temperature, -1)

		return [(inputs[0] ** temperature) * inputs[1]]
		
	def _compute_output_shape(self, input_shape):
		if len(input_shape) != 2:
			raise Exception("Proximity Regularization Layer expects two inputs (plus optional temperature parameter)")
			
		return input_shape[0]
	

class NeuronSelector(Layer):
	def __init__(self, indices, **kwargs):
		super(NeuronSelector, self).__init__(**kwargs)
		self.indices = indices

	def call(self, inputs):
		return K.stack([inputs[:,i] for i in self.indices], axis = -1)
	
	def compute_output_shape(self, input_shape):
		return input_shape[:-1] + (len(self.indices),)


class Argmax(Layer):
	def call(self, inputs):
		return K.one_hot(K.argmax(inputs, axis = -1), K.shape(inputs)[-1])


class _Optimizer:
	def __init__(self, indices, model, weightdir, csvdir, datapath, dictionary, modality, length, projection, verbose):
		self.with_projection = projection
		self.original_model = model
		self.dictionary = dictionary
		self.modality = modality
		self.length = length
		self.verbose = verbose
		self.indices = indices
		self.datapath = datapath
		self.csvpath = os.path.join(csvdir, self.name + "_" + 
			"_".join(str(i) for i in indices) + "_" + modality + "_P" * int(projection) + ".csv")
		self.weightpath = os.path.join(weightdir, self.name + "_" +
			"_".join(str(i) for i in indices) + "_" + modality + "_P" * int(projection) + ".pkl")
		self._check_indices()
	
	def _check_indices(self):
		if not hasattr(self.indices, "__len__") or hasattr(self.indices[0], "__len__"):
			raise Exception("All optimizers except for OptimizerBlackboxFull require a simple list of indices: [1,2,6, ...]")
		
	def build(self):
		self._prep()
		self._build()
		self._assemble()
	
	def _prep(self):
		if isinstance(self.original_model, str):
			self.original_model = load_model(self.original_model, custom_objects = CUSTOM_OBJECTS)
		
		if isinstance(self.dictionary, str):
			with open(self.dictionary, "rb") as handle:
				self.dictionary = _pickle.load(handle)
		
		self.rev_dictionary = {self.dictionary[key]: key for key in self.dictionary}
		
		orig_emb = self.original_model.get_layer("emb")
		orig_enc = self.original_model.get_layer("enc_" + self.modality[0])
		
	
		self.weights = {}
		self.weights["emb"] = orig_emb.get_weights()
		self.weights["enc"] = orig_enc.get_weights()
		
		enc_config = orig_enc.get_config()
		enc_config["return_sequences"] = False
		emb_config = orig_emb.get_config()
		emb_config["mask_zero"] = False

		self.configs = {}
		self.configs["emb"] = emb_config
		self.configs["enc"] = enc_config

		if self.with_projection:
			orig_proj = self.original_model.get_layer(self.modality[0])
			self.weights["proj"] = orig_proj.get_weights()

			if isinstance(orig_proj, TimeDistributed):
				self.configs["proj"] = orig_proj.layer.get_config()
			else:
				self.configs["proj"] = orig_proj.get_config()
			
			if self.modality == "TL":
				self.configs["proj"]["activation"] = "linear"

		for key in self.configs:
			self.configs[key]["trainable"] = False
			del self.configs[key]["name"]
	
		self.encoder = orig_enc.__class__(**self.configs["enc"], weights = self.weights["enc"], name = "enc_exp")
		if self.with_projection:
			self.projection = Dense(**self.configs["proj"], weights = self.weights["proj"], name = "proj_exp")
		
		self.selector = NeuronSelector(self.indices, name = "selector")

class _OptimizerBlackbox(_Optimizer):
	def __init__(self, batchsize = 512, **kwargs):
		super(_OptimizerBlackbox, self).__init__(**kwargs)
		self.batchsize = batchsize
	
	def _prep(self):
		super(_OptimizerBlackbox, self)._prep()

	def _build(self):
		self.model = Sequential()
		self.model.add(self.embedding)
		self.model.add(self.encoder)
		if self.with_projection:
			self.model.add(self.projection)
				
	def _assemble(self):
		if self.verbose > 1:
			self.model.summary()

class OptimizerBlackboxFull(_OptimizerBlackbox):
	name = "full"

	def _check_indices(self):
		if not hasattr(self.indices, "__len__") or not hasattr(self.indices[0], "__len__"):
			raise Exception("OptimizerBlackboxFull requires a nested list of indices: [[0,1], [1,2,6], ...]")

	def _prep(self):
		super(OptimizerBlackboxFull, self)._prep()
		self.configs["emb"]["mask_zero"] = True
		self.embedding = Embedding(**self.configs["emb"], weights = self.weights["emb"], name = "emb_exp")

		self.ngrams = set()
		for dataset in ("train", "test", "val"):
			with open(os.path.join(self.datapath, dataset + ".txt")) as handle:
				for line in handle:
					words = line.strip().split()
					words = [self.dictionary.get(w, self.dictionary["<UNK>"]) for w in words]
					
					if len(words) < self.length:
						words = [0] * (self.length - len(words)) + words

					for i in range(0, len(words) - self.length + 1):
						self.ngrams.add(tuple(words[i:i+self.length]))

		self.ngrams = np.array(list(self.ngrams))

	def train(self):
		self.best_ngrams = [None for _ in range(len(self.indices))]
		self.best_scores = [-np.inf for _ in range(len(self.indices))]

		batchrange = range(0, self.ngrams.shape[0], self.batchsize)
		
		if self.verbose > 1:
			bar = ProgressBar()
			batchrange = bar(batchrange)

		for i in batchrange:
			batch = self.ngrams[i:min(self.ngrams.shape[0], i + self.batchsize)]
			out = self.model.predict_on_batch(batch)
			
			for j, index_group in enumerate(self.indices):
				scores = np.mean([out[:,i] for i in index_group], axis = 0)
				if self.best_scores[j] < scores.max():
					self.best_scores[j] = scores.max()
					self.best_ngrams[j] = self.ngrams[scores.argmax() + i]
			
	def get_best_ngrams(self):
		return [" ".join(self.rev_dictionary[w] for w in ngram) for ngram in self.best_ngrams]
	
	def score_best_ngrams(self):
		return self.best_scores	
	
		
class OptimizerBlackboxGrid(_OptimizerBlackbox):
	name = "grid"

	def _prep(self):
		super(OptimizerBlackboxGrid, self)._prep()
		self.embedding = Embedding(**self.configs["emb"], weights = self.weights["emb"], name = "emb_exp")
		self.mean = Lambda(lambda x:K.mean(x, axis = -1), output_shape = lambda shape:shape[:-1], name = "mean")
	
	def _build(self):
		super(OptimizerBlackboxGrid, self)._build()
		self.model.add(self.selector)
		self.model.add(self.mean)

	def train(self):
		self.optimal_input = np.ones((self.length,), dtype = int) * 3
		self.best_score = -np.inf

		counter = 0
		change = True
		while change and counter < 200:
			change = False
			counter += 1
			for position in reversed(range(self.length)):
				for i in range(3, len(self.dictionary), self.batchsize): # jump over <BEG>, <END>, <UNK>
					current_batchsize = min(self.batchsize, len(self.dictionary) - i)
					current_input = np.tile(self.optimal_input, (current_batchsize, 1))
					current_input[:,position] = range(i, i+current_batchsize)
					scores = self.model.predict_on_batch(current_input)
					if scores.max() > self.best_score:
						self.best_score = scores.max()
						self.optimal_input[position] = scores.argmax() + i
						change = True

	def get_best_ngram(self):
		return " ".join(self.rev_dictionary[w] for w in self.optimal_input)

	def score_best_ngram(self):
		return self.best_score

class _OptimizerParametrical(_Optimizer):

	def __init__(self, batchsize = 1, **kwargs):
		super(_OptimizerParametrical, self).__init__(**kwargs)
		self.calc_hard = True
		self.batchsize = batchsize
	
	def _base_generator(self):
		dummy_in = np.tile(np.arange(self.length), (self.batchsize, 1))
		while True:
			yield({"i_dummy": dummy_in}, [])

	def _get_generator(self):
		base_generator = self._base_generator()
		while True:
			yield(next(base_generator))
	
	def _temperature_generator(self):
		if self.annealing_factor > 1:
			start_temperature = self.min_temperature
			end_temperature = self.max_temperature
			monitor_op = np.greater
		else:
			start_temperature = self.max_temperature
			end_temperature = self.min_temperature
			monitor_op = np.less

		temperature = start_temperature
		while True:
			yield(temperature)
			temperature *= self.annealing_factor
			if monitor_op(temperature, end_temperature):
				temperature = end_temperature
			
	
	def _prep(self):
		super(_OptimizerParametrical, self)._prep()
		self.dummy_input = Input((self.length,), name = "i_dummy")
		self.inputs = [self.dummy_input]
		self.outputs = []
		self.dummy_target = K.constant([[0]])

	def _assemble(self):		
		self.losses = {}
		loss_weights = {}
		target_tensors = {}

		for tensor in self.outputs:
			name = tensor._keras_history[0].name
			
			if name.startswith("o_"): continue
			
			target_tensors[name] = K.constant([[0]])

			if name == "L": 
				self.losses[name] = maximizer_loss
				loss_weights[name] = 1
			else:
				self.losses[name] = report_loss
				loss_weights[name] = 0
		
		assert sum(loss_weights.values()) == 1

		self.model = Model(self.inputs, self.outputs)
		self.model.compile(optimizer = "adam", loss = self.losses, 
			loss_weights = loss_weights, target_tensors = target_tensors)
	
		assert len(self.model.trainable_weights) == 1

		if self.verbose > 1:
			self.model.summary()

	def train(self):
		generator = self._get_generator()
		
		if self.calc_hard:
			monitor = "h_loss"
		else:
			monitor = "s_loss"

		callbacks = []
		callbacks.append(CSVLogger(self.csvpath))
		callbacks.append(EarlyStopping(monitor = monitor, mode = "max", patience = self.patience, verbose = self.verbose))
		callbacks.append(KeepBestModelCallback(monitor = monitor, mode = "max", verbose = self.verbose))

		if self.verbose > 1:
			callbacks.append(BestStringCallback(self, generator))
			
		if self.verbose > 2:
			self.model.stateful_metric_names.append("loss")
			for key in self.losses:
				self.model.stateful_metric_names.append(key + "_loss")
			fit_verbose = 1
		else:
			fit_verbose = 0
		
		self.model.fit_generator(generator, steps_per_epoch = 200, epochs = 1000, callbacks = callbacks, verbose = fit_verbose)
		self._store_weights()

	def score_best_ngram(self):
                if self.calc_hard:
                        generator = self._get_generator()
                        outputs = self.model.predict_on_batch(next(generator)[0])
                        index = self.model.output_names.index("h")
                        return np.mean(outputs[index])
                else:
                        raise Exception("calc_hard must be enabled to calculate score")
	
	def get_best_ngram(self):
		return " ".join(self.rev_dictionary[w] for w in self._get_best_ngram())

class _OptimizerProbabilities(_OptimizerParametrical):
	def __init__(self, **kwargs):
		super(_OptimizerProbabilities, self).__init__(**kwargs)

	def _prep(self):
		super(_OptimizerProbabilities, self)._prep()
		
		self.argmax = Argmax(name = "argmax")
		
		tmp = Dense(units = self.configs["emb"]["output_dim"], activation = "linear", use_bias = False)
		self.embedding = TimeDistributed(tmp, name = "emb_exp", trainable = False, weights = self.weights["emb"])

		self.logits = Embedding(input_dim = self.length, output_dim = len(self.dictionary), name = "logits")
	
	def _store_weights(self):
		with open(self.weightpath, "wb") as handle:
			_pickle.dump(self.logits.get_weights()[0], handle)

	def _get_output(self, probabilities):
		embedded = self.embedding(probabilities)
		encoded = self.encoder(embedded)
		if self.with_projection:
			encoded = self.projection(encoded)
		return self.selector(encoded)	
			
	def _build(self):
	
		logits = self.logits(self.dummy_input)

		selection_soft = self._get_output(self._get_probabilities(logits))
		
		self.outputs.append(Name("s")(selection_soft))
		self.outputs.append(Name("L")(selection_soft))
		self.outputs.append(Name("o_logits")(logits))
		
		if self.calc_hard:
			argmax = self.argmax(logits)
			self.outputs.append(Name("o_argmax")(argmax))

			selection_hard = self._get_output(argmax)
			self.outputs.append(Name("h")(selection_hard))
	
	def _get_best_ngram(self):
		return self.logits.get_weights()[0].argmax(axis = -1)
	
class _OptimizerEmbedding(_OptimizerParametrical):
	def __init__(self, **kwargs):
		super(_OptimizerEmbedding, self).__init__(**kwargs)
	
	def _store_weights(self):
		with open(self.weightpath, "wb") as handle:
			_pickle.dump(self.embedding.get_weights()[0], handle)
	
	def _prep(self):
		super(_OptimizerEmbedding, self)._prep()
		self.orig_emb = K.constant(self.weights["emb"][0])

		self.embedding = Embedding(\
			input_dim = self.length, 
			output_dim = self.configs["emb"]["output_dim"],
			name = "emb_exp")

		if self.calc_hard:
			self.snap = SnapToClosest(self.orig_emb, mode = "max", name = "snap")
			self.cosine = PairwiseCosines(self.orig_emb, name = "cosine")
			self.max = Lambda(lambda x:K.max(x, axis = -1), output_shape = lambda shape:shape[:-1], name = "max")

	def _get_output(self, embedded):
		encoded = self.encoder(embedded)
		if self.with_projection:
			encoded = self.projection(encoded)
		return self.selector(encoded)

			
	
	def _get_best_ngram(self):
		similarities = cosine_similarity(self.embedding.get_weights()[0], self.weights["emb"][0])
		return similarities.argmax(-1)

class OptimizerEmbeddingFree(_OptimizerEmbedding):
	name = "embfree"
	patience = 15

	def _build(self):
		
		embedded = self.embedding(self.dummy_input)
		selection_soft = self._get_output(embedded)

		self.outputs.append(Name("s")(selection_soft))
		self.outputs.append(Name("L")(selection_soft))
		self.outputs.append(Name("o_emb")(embedded))

		if self.calc_hard:
			cosines = self.cosine(embedded) # (length, vocab)
			maxcosines = self.max(cosines)
			
			self.outputs.append(Name("c")(maxcosines))
			
			selection_hard = self._get_output(self.snap(cosines))
			self.outputs.append(Name("h")(selection_hard))
		

class OptimizerEmbeddingRegularized(_OptimizerEmbedding):
	name = "embreg"
	patience = 30

	def __init__(self, max_temperature = 2, min_temperature = 0.01, annealing_factor = 1.001, **kwargs):
		super(OptimizerEmbeddingRegularized, self).__init__(**kwargs)
		self.max_temperature = max_temperature
		self.min_temperature = min_temperature
		self.annealing_factor = annealing_factor


	def _get_generator(self):
		base_generator = self._base_generator()
		temperature_generator = self._temperature_generator()
		
		while True:
			dummy_in, dummy_out = next(base_generator)
			temperature = next(temperature_generator)
			dummy_in["i_temp"] = np.ones((self.batchsize, 1)) * temperature

			yield(dummy_in, dummy_out)
	
	def _prep(self):
		super(OptimizerEmbeddingRegularized, self)._prep()
		
		if not hasattr(self, "cosine"):
			self.cosine = PairwiseCosines(self.orig_emb, name = "cosine")
		
		if not hasattr(self, "max"):
			self.max = Lambda(lambda x:K.max(x, axis = -1), output_shape = lambda shape:shape[:-1], name = "max")
		
		self.proximityreg = ProximityRegularizationLayer(name = "proxreg")
	
		self.mean = Lambda(lambda x:K.mean(x, axis = -1, keepdims = True), output_shape = lambda shape:shape[:-1], name = "mean")
		self.temperature = Input((1,), name = "i_temp")
		self.relu = Activation("relu", name = "relu")

	def _build(self):
		embedded = self.embedding(self.dummy_input)		
		selection_soft = self._get_output(embedded)

		self.outputs.append(Name("s")(selection_soft))
		self.outputs.append(Name("o_emb")(embedded))

		cosines = self.cosine(embedded) # (length, vocab)
		maxcosines = self.max(cosines)
		
		self.outputs.append(Name("c")(maxcosines))
			
		if self.calc_hard:
			selection_hard = self._get_output(self.snap(cosines))
			self.outputs.append(Name("h")(selection_hard))

		proximityreg = self.proximityreg([self.mean(self.relu(maxcosines)), selection_soft, self.temperature])
		
		self.outputs.append(Name("sc")(proximityreg))
		self.outputs.append(Name("L")(proximityreg))
		
		self.inputs.append(self.temperature)
		self.outputs.append(Name("t")(self.temperature))

	
		
class OptimizerGumbel(_OptimizerProbabilities):
	name = "gumbel"
	patience = 200

	def __init__(self, max_temperature = 2.0, min_temperature = 0.1, annealing_factor = 0.99995, batchsize = 64, **kwargs):
		super(OptimizerGumbel, self).__init__(batchsize = batchsize, **kwargs)
		self.max_temperature = max_temperature
		self.min_temperature = min_temperature
		self.annealing_factor = annealing_factor

	def _prep(self):
		super(OptimizerGumbel, self)._prep()
		self.softmax = Activation("softmax", name = "softmax")
		self.gumbel = Gumbel(name = "gumbel")
		self.temperature = Input((1,), name = "i_temp")

	def _get_probabilities(self, logits):
		softmax = self.softmax(logits)
		gumbel = self.gumbel([softmax, self.temperature])
		
		self.outputs.append(Name("o_gumbel")(gumbel))
		self.outputs.append(Name("o_softmax")(softmax))
		self.outputs.append(Name("t")(self.temperature))

		self.inputs.append(self.temperature)
		return gumbel

	def _get_generator(self):
		base_generator = self._base_generator()
		temperature_generator = self._temperature_generator()
		
		while True:
			dummy_in, dummy_out = next(base_generator)
			temperature = next(temperature_generator)
			dummy_in["i_temp"] = np.ones((self.batchsize, 1)) * temperature

			yield(dummy_in, dummy_out)


class OptimizerSoftmax(_OptimizerProbabilities):
	name = "softmax"
	patience = 100
	
	def _prep(self):
		super(OptimizerSoftmax, self)._prep()
		self.softmax = Activation("softmax", name = "softmax")

	def _get_probabilities(self, logits):
		softmax = self.softmax(logits)
		self.outputs.append(Name(name = "o_softmax")(softmax))
		return softmax

class OptimizerLogits(_OptimizerProbabilities):
	name = "logits"
	patience = 100
	def _get_probabilities(self, logits):
		return logits


def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument(dest = "optimizer", type = str, 
		choices = ("grid", "full", "embreg", "embfree", "logits", "gumbel", "softmax"))
	parser.add_argument(dest = "modality", type = str, choices = ("T", "TL", "I"))

	parser.add_argument("--projection", dest = "projection", action = "store_true")
	parser.add_argument("--reverse", dest = "reverse", action = "store_true")
	parser.add_argument("--model", type = str, default = MODELPATH, dest = "model")
	parser.add_argument("--dictionary", type = str, default = DICTIONARYPATH, dest = "dictionary")
	parser.add_argument("--datapath", type = str, default = DATAPATH, dest = "datapath")
	parser.add_argument("--csvdir", type = str, default = CSVDIR, dest = "csvdir")
	parser.add_argument("--weightdir", type = str, default = WEIGHTDIR, dest = "weightdir")
	parser.add_argument("--length", type = int, default = 5, dest = "length")
	parser.add_argument("--verbose", type = int, default = 0, dest = "verbose")
	args = parser.parse_args()
	return vars(args)

def get_optimizer_class(optimizer):
	if optimizer == "full":
		return OptimizerBlackboxFull
	if optimizer == "grid":
		return OptimizerBlackboxGrid
	if optimizer == "embreg":
		return OptimizerEmbeddingRegularized
	if optimizer == "embfree":
		return OptimizerEmbeddingFree
	if optimizer == "gumbel":
		return OptimizerGumbel
	if optimizer == "softmax":
		return OptimizerSoftmax
	if optimizer == "logits":
		return OptimizerLogits
	else:
		raise Exception("Unknown optimizer:", optimizer)

def get_indices(args, num_draws = 80, max_index = 1024, max_num = 1):
	state = np.random.RandomState(123)

	options = list(range(max_index))
	indices = set()

	while len(indices) < num_draws:
		num_neurons = state.randint(low = 1, high = max_num + 1)
		choice = state.choice(options, size = (num_neurons,), replace = False)
		indices.add(tuple(sorted(choice)))

	indices = tuple(sorted(indices, key = lambda x:x[0]))
	if args.pop("reverse"):
		indices = tuple(reversed(indices))
	return indices

def pretty(indices, ngram, targetword, score):
	print("OPT", end = "\t")
	print(" ".join(str(i) for i in indices), end = "\t")
	print(ngram, end = "\t")
	print(targetword, end = "\t")
	print(score, flush = True)


if __name__ == "__main__":

	args = parse_arguments()

	optimizer = args.pop("optimizer")
	optimizer_class = get_optimizer_class(optimizer)

	INDICES = get_indices(args)
	K.clear_session()
	targetword = "N"
	if optimizer == "full":
		optimizer = optimizer_class(indices = INDICES, **args)
		optimizer.build()
		optimizer.train()

		
		ngrams = optimizer.get_best_ngrams()
		scores = optimizer.score_best_ngrams()
		for indices, ngram, score in zip(INDICES, ngrams, scores):
			if args["modality"][0] == "T" and len(indices) == 1 and args["projection"]:
				targetword = optimizer.rev_dictionary[indices[0]]

			pretty(indices, ngram, targetword, score)

	else:
		for indices in INDICES:
			optimizer = optimizer_class(indices = indices, **args)
			optimizer.build()
			optimizer.train()

			if args["modality"][0] == "T" and len(indices) == 1 and args["projection"]:
				targetword = optimizer.rev_dictionary[indices[0]]
			
			pretty(indices, optimizer.get_best_ngram(), targetword, optimizer.score_best_ngram())
			K.clear_session()

	K.clear_session()	
	




