\documentclass[portrait,a0]{a0poster}
\usepackage{graphicx}
\usepackage{color}
\usepackage[margin=0cm]{geometry}
\usepackage{setspace}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{colortbl}
\usepackage{wasysym}
\usepackage{multirow, rotating}

\definecolor{lmu-darkgreen}{RGB}{0, 148, 64}
\definecolor{lmu-darkred}{RGB}{200, 0, 0}
\definecolor{lmu-darkgray}{RGB}{192, 192, 192}
\definecolor{lmu-lightgray}{RGB}{215, 215, 215}
\setlength\parindent{0pt}

\usepackage{tikz}
\usetikzlibrary{positioning}

\usepackage{changepage}
\usepackage[pscoord]{eso-pic}

\newcommand{\gr}{\rowcolor{lmu-lightgray}}
\newcommand{\rot}[1]{\rotatebox[origin=c]{90}{#1}}

\begin{document}
\fcolorbox{lmu-darkgreen}{lmu-darkgreen}{
	\begin{minipage}{1.02\textwidth}
		\centering
		\vspace{2.2cm}
		{\Huge \bf{Interpretable Textual Neuron Representations for NLP}}

	\begin{minipage}{\textwidth}
		\centering
		\vspace{1cm}
		{\Large Nina Poerner, Benjamin Roth, Hinrich Sch{\"utze}}
		\vspace{5mm}
	\end{minipage}
	\begin{minipage}{\textwidth}
		\centering
		{\texttt{poerner@cis.uni-muenchen.de}}
		\vspace{0.5cm}
	\end{minipage}
	\end{minipage}
}

\vspace{3cm}

\hspace{4cm}
\begin{minipage}[t]{.24\textwidth}
\centering {\centering \huge \textbf{Motivation}}
\vspace{5mm}
\begin{itemize}
\item Neural networks neurons are typically thought of as complex \textbf{feature detectors}
\item Contrary to shallow models, we do not know the \textbf{mapping} between neurons and features
\item Basic assumption of \textbf{Input Optimization}: We can represent a neuron by the \textbf{input that maximizes its activation}
\item e.g., a neuron that is maximally activated by ``wonderful superb great amazing'' is interpretable as a detector for positive sentiment
\item \textbf{How to find this maximally activating input?}
\item[]
\end{itemize}

\end{minipage}
\hfill
\begin{minipage}[t]{.65\textwidth}
  \begin{minipage}{\textwidth}
    \centering
    \huge \textbf{Input Optimization in Computer Vision\phantom{...}}
    \vspace{2cm}
  \end{minipage}

  \begin{minipage}{\textwidth}
    \begin{minipage}[t]{.45\textwidth}
      
      \includegraphics[scale=.9]{simonyan}
      
      \vspace{1cm}
      
      \cite{simonyan2013deep} use gradient ascent to find image $\hat{I}$,
      
      such that $\hat{I} = \mathrm{argmax}_I[S_c(I) - \lambda ||I||_2] $
      
      Here, $S_c$ are top-level features (= output class scores)
      
      $I$ is initialized with noise
      
    \end{minipage}
    \begin{minipage}[t]{.6\textwidth}
    
      \includegraphics[scale=1.1]{deepdream}
      
      \vspace{1cm}
   
      \cite[Google DeepDream]{mordvintsev2015deepdream}
      
      Gradient ascent, initialized with existing image
      
      Targets are groups of high- or low-level neurons (e.g., edge detectors)
      
      {\footnotesize \url{https://ai.googleblog.com/2015/06/inceptionism-going-deeper-into-neural.html}}

    \end{minipage}
    
    \vspace{1cm}

  \end{minipage}
  \hspace{4cm}
\end{minipage}

\hspace{4cm}
\vspace{1.5cm}
\setlength{\fboxrule}{5mm}
\fcolorbox{lmu-darkred}{lmu-darkgray}{
\begin{minipage}{.87\textwidth}
\vspace{1cm}
\centering \textbf{\Large Input Optimization in a nutshell}
\vspace{5mm}

\large
Select target neuron(s) $\rightarrow$ Initialize trainable ``input'' $\mathbf{X}$ $\rightarrow$ Keep all other network weights fixed $\rightarrow$ Apply favourite optimizer to maximize target neuron activation(s)
\vspace{1cm}
\end{minipage}
}

\fcolorbox{lmu-darkgreen}{lmu-darkgreen}{
	\begin{minipage}{1.02\textwidth}
	\vspace{1cm}
	\centering
	\huge \textbf{Applying Input Optimization to NLP}
	\vspace{1cm}
	\end{minipage}
}


\vspace{1cm}
\hspace{4cm}
\begin{minipage}{.88\textwidth}
  \begin{minipage}{\textwidth}
    
    \vspace{2cm}
    \begin{minipage}{\textwidth}
      \begin{minipage}[t]{.36\textwidth}
      \centering \textbf{\large \color{lmu-darkred}{! Problem: Text inputs are not differentiable !}}
      
      \vspace{2cm}
      {\Large \textbf{Solution 1: Corpus Search}}
      \begin{itemize}
        \item Search in an existing corpus, e.g., the training corpus, for optimal n-grams \cite{kadar2017representation} $\rightarrow$ no need for gradients 
        \item Can only cover the space of existing inputs. But as we see in Computer Vision, optimal inputs may be more abstract than existing inputs.
        \item Used as baseline below.
      \end{itemize}
        
        \vspace{2cm}
        
        
        {\Large \textbf{Solution 2: Embedding Optimization}}
        
        \begin{itemize}
          \item Find vectors in the embedding space that maximize the objective, then find their closest real-word neighbors.
          \item $\hat{x}_1 \ldots \hat{x}_T = \underset{}{\mathrm{argmin}}[\sum_{t=1}^T \mathrm{cos}(\mathbf{\hat{e}_t}, \mathbf{M}_{x_t})]; \quad \mathbf{\hat{e}_1} \ldots \mathbf{\hat{e}_T} = \underset{\mathbf{e_1} \ldots \mathbf{e_T}}{\mathrm{argmax}}[f(\mathbf{E})]$
          \begin{itemize}
          \item where $f(\mathbf{E})$ is the activation of our target neuron(s) when forward-feeding the embeddings $\mathbf{e}_1 \ldots \mathbf{e}_T$, $M$ is the embedding matrix of all known real words, and n-gram length $T$ is fixed beforehand
          \end{itemize}
          \item \textbf{Problem:} Empirically, the optimal embeddings tend to be very dissimilar from their closest real-word neighbors \textbf{\frownie{}}
        \end{itemize}
        
        \begin{tikzpicture}
        \node [draw=none] at(0,0) {};
        \draw [->] (2,2) -- (5,5) node {queen};
        \draw [->] (2,2) -- (6,4) node {king};
        \draw [->] (2,2) -- (1,3) node {the};
        \draw [->, red] (2,2) -- (6,3.2) node {$\mathbf{e_2}$};
        \draw [->, red] (2,2) -- (1, 2.3) node {$\mathbf{e_1}$};
        \node at (3,1) {$\rightarrow \hat{X}$ = ``the king''};
        \end{tikzpicture}
        \hspace{2cm}
        \includegraphics[scale=1.1]{embfree}
        
        \vspace{2cm}
        
        
        {\Large \textbf{Solution 3: Word Probability Optimization}}
        \begin{itemize}
          \item The embedding operation is equivalent to multiplying a matrix of one-hot vectors $\mathbf{X}$ with $\mathbf{M}$. If we drop the requirement that $\mathbf{X}$ be one-hot, we can optimize $\mathbf{X}$ while keeping $\mathbf{M}$ fixed.
          \item \textbf{logits}: $\mathbf{\hat{x}_1} \ldots \mathbf{\hat{x}_T} = \underset{\mathbf{X}}{\mathrm{argmax}}[f(\mathbf{X} \mathbf{M})]$
          \begin{itemize}
            \item values in $\mathbf{X}$ are free to become large or negative, and therefore unlike the one-hot distributions seen in training \textbf{\frownie{}}
        \end{itemize}
          \item \textbf{softmax}: $\mathbf{\hat{x}_1} \ldots \mathbf{\hat{x}_T} = \underset{\mathbf{X}}{\mathrm{argmax}}[f(\mathbf{P^\mathrm{smx}} \mathbf{M})]$; \quad $\mathbf{p^\mathrm{smx}_t} = \mathrm{softmax}(\mathbf{x_t})$
          \begin{itemize}
            \item $\mathbf{p^\mathrm{smx}}$ may be smooth and therefore unlike a one-hot distribution \textbf{\frownie{}}
          \end{itemize}
          \item \textbf{gumbel softmax}: $\mathbf{\hat{x}_1} \ldots \mathbf{\hat{x}_T} = \underset{\mathbf{X}}{\mathrm{argmax}}[f(\mathbf{P^\mathrm{gbl}} \mathbf{M})]$
          \begin{itemize}
            \item $\mathbf{p}^\mathrm{gbl}_t = \mathrm{softmax}[\tau^{-1} (\mathrm{log}(\mathbf{p}^\mathrm{smx}_t) + \mathbf{g}_t)]; \quad g_{t,v} \sim -\mathrm{log}(-\mathrm{log}(\mathcal{U}(0,1)))$
            \item \cite{maddison2017concrete}, \cite{jang2017categorical}
            \item $\mathbf{p^\mathrm{gbl}}$ typically concentrates probability mass in one or few words \textbf{\smiley{}}
          \end{itemize}
        \end{itemize}
        \vspace{5mm}
        \includegraphics[scale=1.1]{softmax}
        \hfill
        \includegraphics[scale=1.1]{gumbel}
      
      \end{minipage}
      \hfill
      \begin{minipage}[t]{.55\textwidth}
      \centering
      {\huge \textbf{Experiments}}
      \vspace{1.5cm}
      
      {\Large \textbf{Neural network}}
      \begin{itemize}
        \item Imaginet architecture from \cite{kadar2017representation}, c.f., \cite{chrupala2017representations}
        \item Trained on 566435 MSCOCO image-caption pairs.
        \item Joint word embedding (size 1024), two separate unidirectional GRUs (size 1024) with separate projection layers.
        \begin{itemize}
          \item TEXTUAL GRU: standard word-level language model.
          \item VISUAL GRU: learns to produce a representation that is close to the representation of the associated image (as computed by an external CNN).
        \end{itemize}
      \end{itemize}
      
      \vspace{1cm}

      {\Large \textbf{Quantitative evaluation}}
      \begin{itemize}
        \item Evaluation goal: find 5-gram that maximizes target neuron activation(s).
        \item In projection layers, maximize 180 randomly chosen neurons.
        \item In GRU hidden states, maximize mean $\mathrm{tanh}$ activation achieved for 81 groups of highly correlated neurons. 
        \begin{itemize}
          \item Groups are discovered by hierarchical clustering.
        \end{itemize}
        \item \textbf{The higher the activation, the better the representation.}
        \item Note that we use purely symbolic 5-grams at test time, and not their differentiable proxies.
        \begin{itemize}
          \item Embedding optimization: Closest real-word neighbors of optimal embeddings.
          \item Word probability optimization: $\mathrm{argmax}$ over the vocabulary axis of $\mathbf{\hat{X}}$.
        \end{itemize}
        \item \textbf{Significance tests}
        \begin{itemize}
          \item Pairwise t-tests on the differences between corpus search and gumbel softmax (paired by neuron / neuron group).
          \item $p < 0.0001$ for all combinations of visual/textual and projection/hidden.
        \end{itemize}
      \end{itemize}

      \vspace{1cm}
      
      \hspace{1cm}
      \includegraphics[scale=2.2]{figure_proj}
      \hfill      
      \includegraphics[scale=2.2]{figure_hid}
      \hspace{1cm}
      
      \vspace{2cm}

      {\Large \textbf{Qualitative evaluation: examples}}
      \vspace{1cm}      
      
      {\small
      \begin{tabular}{l||lr||lr}
      \multicolumn{1}{c}{} & \multicolumn{2}{c}{visual projection layer} & \multicolumn{2}{c}{language model projection layer} \\[2pt] \hline \hline
corpus search & trains at the train track & 39.69 & a white bed sitting between	& 10.56 \\
gumbel softmax & wagons stain fading locomotives trains & 45.50 & unshaven glazed streetlight pole between & 13.93 \\ \hline \hline
\multicolumn{3}{c}{neuron 124} & \multicolumn{2}{c}{neuron 21 (``two'')} \\[2pt]
\hline \hline
corpus search & pizza a sandwich and appetizers  & 48.44 & finish line at a horse & 9.45 \\
gumbel softmax & fangs calzone raspberries sandwhich pizzas & 64.46 & horsed horseback motocycles enthusiast they& 13.32 \\ \hline \hline
\multicolumn{3}{c}{neuron 315} & \multicolumn{2}{c}{neuron 522 (``race'')} \\[2pt]
\hline \hline
corpus search & goalie catching a soccer ball & 35.72 & a woman sitting under an & 13.28 \\ 
gumbel softmax & irritated coaching partners volleyball playground & 43.16 & campbell lawn raincoat under an & 17.55 \\ \hline \hline
\multicolumn{3}{c}{neuron 484} & \multicolumn{2}{c}{neuron 314 (``umbrella'')} \\[2pt]
\hline \hline
corpus search & fighter jet flying in formation & 31.25 & the view through a car & 10.42 \\
gumbel softmax & propelleor phrases jetliners treetops flight & 37.82 & logging jeep watch through cracked & 14.87 \\ \hline \hline 
\multicolumn{3}{c}{neuron 657} & \multicolumn{2}{c}{neuron 957 (``windshield'')} \\[2pt] \hline \hline
corpus search & goalie outfit throwing a ball & 65.00 & a giraffe looks to its & 10.64 \\
gumbel softmax & footplate goalie pitchers racecourse bat & 75.30 & fest stares stares to their & 13.22 \\ \hline \hline
\multicolumn{3}{c}{neuron 1006} & \multicolumn{2}{c}{neuron 973 (``left'')} \\[2pt]
\hline \hline
\multicolumn{5}{c}{Optimal 5-grams with target neuron activations according to corpus search and gumbel softmax.} 
\end{tabular}
}
      
      \end{minipage}
    \end{minipage}
  \end{minipage}
  \hspace{1cm}
\end{minipage}


\vfill
\fcolorbox{lmu-darkgreen}{lmu-darkgreen}{
\begin{minipage}{1.02\textwidth}
\vspace{1cm}
\hspace{2cm}
\begin{minipage}{.9\textwidth}
{\small
\bibliographystyle{apalike}
\bibliography{../Paper/emnlp2018}
\vspace{1cm}
}
\end{minipage}

\hspace{2cm} Code available at \url{https://github.com/NPoe/input-optimization-nlp} \hfill Analyzing and Interpreting Neural Networks for NLP. Workshop @ EMNLP 2018. 01 November 2018. \hspace{3cm}
\end{minipage}
}
\end{document}
